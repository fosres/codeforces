#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

int main(void)
{
	unsigned afreq['z'-'a'+1], bfreq['z'-'a'+1];

	unsigned tfreq['z'-'a'+1];

	memset(tfreq,0x0,('z'-'a'+1)*sizeof(unsigned));

	memset(afreq,0x0,('z'-'a'+1)*sizeof(unsigned));

	memset(bfreq,0x0,('z'-'a'+1)*sizeof(unsigned));

	unsigned char bstring[100001], *bp = &bstring[0], res[100001], * resp = &res[0]-1;
	
	memset(bstring,0x0,100001*sizeof(unsigned char));

	memset(res,0x0,100001*sizeof(unsigned char));
	
	unsigned excess = 0, temp_excess = 0;

	unsigned char c = 0;
	
	bool IN = 0;
 	
	unsigned i = 0, j = 100000, iprev = 0, jprev = 0, jold = 0, m = 0, n = 0, len = 0, blen = 0;

	while 	(

			( (c = fgetc(stdin)) )

			&&
			
			( c >= 'a' )

			&&

			( c <= 'z' )
			
			&&

			( (++i) <= 100000 )		

		)

	{
		afreq[c-'a']++;
	
	}

	if ( c != 0xa )
	{
		fprintf(stderr,"Error: Improperly formatted standard input.\nFound hex code:0x%.2x\n",c);

		exit(255);
	}

	i = 0;

	while 	(

			( (c = fgetc(stdin)) )

			&&
			
			( c >= 'a' )

			&&

			( c <= 'z' )
			
			&&

			( (++i) <= 100000 )		

		)

	{
		bfreq[c-'a']++;

		if	(
				bfreq[c-'a'] > afreq[c-'a']
			)
		{
			excess++;			
		}

		*bp++ = c;
		
		blen++;
	}

	if 	(
			( c != 0xa)
		
			&&
	
			( c != 0xff)

		)
	{
		fprintf(stderr,"Error: Improperly formatted standard input. Found hex code 0x%.2f\n",c);

		exit(255);
	}

	if	(
			i > 100000
		)
	{
		fprintf(stderr,"Error: Too many digits in string b\n");

		exit(255);
	}

	if	( excess == 0 )
	{
		printf("%s",bstring);

		return 0;
	}

	fprintf(stderr,"excess:%llu\n",excess);

	temp_excess = excess;
	
	bp = &bstring[0];

	memset(tfreq,0x0,('z'-'a'+1)*sizeof(unsigned));

	memcpy(tfreq,bfreq,('z'-'a'+1)*sizeof(unsigned));

	i = 0; j = 100000;

	bool I_INIT = 0;
	
	while ( *bp != 0x0 )
	{
		if	( tfreq[(*bp)-'a'] > afreq[(*bp)-'a'] )
		{
			tfreq[(*bp)-'a']--;

			if	( I_INIT == 0 )
			{
				i = bp - &bstring[0];
				
				I_INIT = 1;	
			}

			j = bp - &bstring[0];

			temp_excess--;

			if	( temp_excess == 0 )
			{
				break;
			}	
		}

		bp++;
	}
	
	memset(tfreq,0x0,('z'-'a'+1)*sizeof(unsigned));

	memcpy(tfreq,bfreq,('z'-'a'+1)*sizeof(unsigned));

	len = j - i + 1;

	bp = &bstring[i];

	jold = j;

	fprintf(stderr,"len: %llu\n",len);

	fprintf(stderr,"i: %llu\n",i);
	
	fprintf(stderr,"j: %llu\n",j);

	temp_excess = excess;

	fprintf(stderr,"temp_excess:%llu\n",temp_excess);

	while 	(
			(temp_excess > 0 )

			&&

			(*bp != 0x0)

			&&

			(bp <= &bstring[jold])
			
		)
	{
		
		if ( tfreq[(*bp) -'a'] > afreq[(*bp) -'a'] )
		{
			if ( IN == 0 )
			{
				IN = 1;

				iprev = i;

				jprev = j;

				i = bp - &bstring[0];
			}

			j = bp - &bstring[0];
			
			tfreq[(*bp) - 'a']--;

			temp_excess--;

			fprintf(stderr,"temp_excess:%llu\n",temp_excess);
			
			if ( temp_excess == 0 )
			{
				break;
			}
		}

		else 	if ( IN == 1 )
		{
			IN = 0;

			temp_excess = excess;
				
			memset(tfreq,0x0,('z'-'a'+1)*sizeof(unsigned));
		
			memcpy(tfreq,bfreq,('z'-'a'+1)*sizeof(unsigned));

			i = iprev;

			j = jprev;

			continue;
		}
		
		bp++;
	}

	if ( temp_excess > 0 )
	{
		i = iprev;

		j = jprev;
	}
	
	fprintf(stderr,"blen: %llu\n",blen);

	fprintf(stderr,"i: %llu\n",i);
	
	fprintf(stderr,"j: %llu\n",j);

	m = 0; n = 0;

	while ( m < i )
	{
		*++resp = bstring[m];

		m++;	
	}

	n = j + 1;
	
	while ( n < blen )
	{
		*++resp = bstring[n];

		n++;
	}

	if 	( res[0] == 0x0 )
	{
		res[0] = '-';
	}

	fprintf(stderr,"%s",res);
	
	return 0;
}
