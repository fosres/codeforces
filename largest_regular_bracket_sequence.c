#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void)
{
	unsigned char input[1000001], *s = &input[0], stack[1000001], *sp = &stack[0]-1, *pp = 0;

	memset(input,0x0,1000001*sizeof(unsigned char));

	memset(stack,0x0,1000001*sizeof(unsigned char));

	unsigned i = 0, count = 0, len = 0, maxlen = 0;

	unsigned char c = 0, IN = 0;

	while 	(
			(c = fgetc(stdin))

			&&

			(
				(c == '(')
				
				||

				(c == ')')

			)

			&&

			( i < 1000000 )
		)

	{
		input[i] = c;

		i++;
	}

	if 	(
			(c != 0xa)
			
			&&

			(c != 0xff)
		)
	{
		fprintf(stderr,"Error: Improperly formatted standard input. Found character with hex code:%.2x\n",c);
	
		exit(255);
	}

	if 	( i > 1000000 )
	{
		fprintf(stderr,"Error: Input too large!\n");

		exit(1);
	}
	
	while (*s != 0x0)
	{
		if (*s == '(')
		{
			*++sp = '(';

			pp = sp;
		}

		else
		{
			if	(
					(pp == 0)

					||

					(pp < &stack[0])
				)
			{
				pp = sp;
			}

			while 	(
					( pp >= &stack[0])
					
					&&

					(*pp == 0x0)
				)
			{
				pp--;
			}

			if	(
					(pp >= &stack[0])
	
					&&

					(*pp == '(')
				)
			{
				*pp-- = 0x0;
			}

			else
			{
				*++sp = ')';

				pp = sp;
			}
		}

		s++;
	}

	pp = &stack[0];

	while	( pp <= sp )
	{
		if	(*pp == 0x0)
		{
			len++;

		}

		else if	( len > maxlen )
		{
			maxlen = len;

			len = 0;

			count = 1;
		}

		else if	( 
				( len == maxlen )

				&& 
				
				(len != 0)
			)
		{
			count++;

			len = 0;
		}

		else
		{
			len = 0;
		}

		pp++;
	}

	if	( len > maxlen )
	{
		maxlen = len;

		len = 0;

		count = 1;
	}

	else if	( 
			( len == maxlen )

			&& 
			
			(len != 0)
		)
	{
		count++;

		len = 0;
	}

	if	( count == 0 )
	{
		printf("0 1");
	}

	else	
	{
		printf("%u %u",2*maxlen,count);
	}

	return 0;
}
