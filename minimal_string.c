#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void minimal(unsigned char s[],unsigned char t[],unsigned char u[],unsigned table[])
{
	unsigned char *sp = &s[0], *tp = &t[0]-1, *up = &u[0]-1;

	unsigned * table_p = &table[0];

	while	(
			(table_p < &table['z'-'a'+1])
		
			&&

			( (*table_p) == 0)
		)
	{
		table_p++;	
	}

	while ( table_p < &table['z'-'a'+1] )
	{
		while	(
				(*table_p > 0)

				&&
	
				(tp >= &t[0])

				&&

				(*tp <= (table_p-&table[0]+'a'))
	
			)

		{
			//fprintf(stderr,"Current letter in freqtable:%c\n",table_p-&table[0]+'a');

			//fprintf(stderr,"Pushing character to u stack and popping t stack:%c\n",*sp);

			*(++up) = *tp--;
		}

		while 	(
				(*table_p > 0)

				&&

				(*sp != 0x0)

				&&

				(*sp > (table_p-&table[0]+'a'))
			)

		{
			//fprintf(stderr,"Current letter in freqtable:%c\n",table_p-&table[0]+'a');

			//fprintf(stderr,"Pushing character to t stack and traversing s:%c\n",*sp);

			table[(*sp)-'a']--;

			*(++tp) = *sp++;
			
		}

		while 	(
				(*table_p > 0)
	
				&&

				(*sp != 0x0)
				
				&&

				(*sp <= (table_p-&table[0]+'a'))
			)
		{
			//fprintf(stderr,"Current letter in freqtable:%c\n",table_p-&table[0]+'a');

			//fprintf(stderr,"Pushing character to u stack and traversing s:%c\n",*sp);
			
			table[(*sp)-'a']--;

			*(++up) = *sp++;
			
		}

		while	(
				(table_p < &table['z'-'a'+1])

				&&
	
				( (*table_p) == 0)
			)	
		{
			table_p++;
		}
	}

	while 	(
			(tp >= &t[0])	
		)
	{
		//fprintf(stderr,"Popping character off of stack t:%c\n",*tp);

		*(++up) = *tp--;
	}
}

int main(void)
{

	unsigned char c = 0;

	unsigned char s[100001], t[100001], u[100001];

	memset(s,0x0,100001*sizeof(unsigned char));
	
	memset(t,0x0,100001*sizeof(unsigned char));
	
	memset(u,0x0,100001*sizeof(unsigned char));

	unsigned table['z'-'a'+1];

	memset(table,0x0,('z'-'a'+1)*sizeof(unsigned));

	unsigned i = 0;

	while 	(
			( (c = fgetc(stdin)) >= 'a')

			&&

			( c <= 'z' )
			
			&&
			
			( i < 100000 )

		)

	{
		s[i] = c;

		table[c -'a']++;

		i++;
	}

	if	(

			(c != 0xa)

			&&

			(c != 0xff)

		)
	{
		fprintf(stderr,"Error: Improperly formatted standard input!\nFound character with hex code:%.2x\n",c);

		exit(255);
	}

/*
	i = 0;
	
	while 	(
			(i < ('z'-'a'+1))
		)
	{
		//fprintf(stderr,"%u ",table[i]);

		i++;
	}	
*/

	//fprintf(stderr,"\n");
			
	minimal(s,t,u,table);

	printf("%s",u);
	
	return 0;
}
