#include <stdio.h>
#include <stdlib.h>
#include <time.h>

unsigned long long int lottery(unsigned long long int x)
{
	unsigned long long int sum = 0;

	sum += (x/100);

	x %= 100;

	sum += (x/20);

	x %= 20;

	sum += (x/10);

	x %= 10;

	sum += (x/5);

	x %= 5;

	sum += (x/1);

	x %= 1;

	return sum;

}

int main(void)
{
	double elapsed = 0;

	clock_t begin = clock(), end = 0;

	unsigned char c = 0;

	unsigned long long int x = 0;
	
	unsigned long long int prev = 0;

	unsigned long long int i = 0;

	while 	(

			(
				(c = fgetc(stdin) ) != EOF
			)

			&&

			( c >= '0')

			&&

			(c <= '9')
		)
	{
		prev = x;

		x *= 10;

		x += (c-'0');
		
		if ( i >= 21 )
		{
			end = clock();

			fprintf(stderr,"Error: Too many digits in integer. Exiting.\n");

			fprintf(stderr,"Time elapsed: %.6f\n",(double)(end-begin)/((double)(CLOCKS_PER_SEC)));

			exit(255);

		}
		
		if ( x < prev )
		{
			end = clock();

			fprintf(stderr,"Error: Integer Overflow Vulnerability detected\n");
	
			fprintf(stderr,"Time elapsed: %.6f\n",(double)(end-begin) / ((double)(CLOCKS_PER_SEC)));

			exit(1);
		}
		
		i++;
	}

	if ( (c != 0xff) && (c != 0xa) )
	{
		end = clock();

		fprintf(stderr,"Error: Improper formatting of standard input detected\n");

		fprintf(stderr,"Time elapsed: %.6f\n",(double)(end-begin) / ((double)(CLOCKS_PER_SEC)));

		exit(1);
	}

	if	(
			( x < 1)
			
			||

			( x > 1000000000 )
	
		)
	{
		end = clock();

		fprintf(stderr,"Error: Input out of bounds\n");

		fprintf(stderr,"Time elapsed: %.6f\n",(double)(end-begin) / ((double)(CLOCKS_PER_SEC)));

		exit(1);

	}

	end = clock();

	printf("%llu\n",lottery(x));

	fprintf(stderr,"Time elapsed: %.6f\n",(double)(end-begin) / ((double)(CLOCKS_PER_SEC)));
	
	return 0;
}
